<?php


class controleur
{

    private $vpdo;
    private $db;

    public function __construct()
    {
        $this->vpdo = new mypdo();
        $this->db = $this->vpdo->connexion;
    }

    public function __get($propriete)
    {
        switch ($propriete) {
            case 'vpdo' :
            {
                return $this->vpdo;
                break;
            }
            case 'db' :
            {

                return $this->db;
                break;
            }
        }
    }

	public function retourne_article($title)
	{

		$retour = '<section>';
		$result = $this->vpdo->liste_article($title);
		if ($result != false) {
			while ($row = $result->fetch(PDO::FETCH_OBJ)) // parcourir chaque ligne sélectionnée
			{
				$retour = $retour . '<div class="card text-white bg-dark m-2" ><div class="card-body">
				<article>

					<h3 class="card-title">'.$row->h3.'</h3>
					<h6 class="card-subtitle">'.$row->nom.' '.$row->prenom.' '.$row->date_redaction.' </h6>
					<div class="lulu" >			
					<p class="card-text">' . $row->corps . '</p></div>
					<button class="lire">cacher</button>
					<button class="cacher">lire la suite</button>

				</article>
				</div></div>';
			}
			$retour = $retour . '</section>';
			return $retour;
		}
	}

    public function retourne_slider()
    {
        $root = $_SERVER['DOCUMENT_ROOT'];

        $images = array_diff(scandir("$root/PPE2-EPSI-2019/image/france/IMAGES"), array('.', '..'));

        $retour = '<div id="slider" class="carousel slide" data-ride="carousel">
 					 <div class="carousel-inner">';

        $first = true;
        foreach ($images as $img) // parcourir chaque image sélectionnée
        {
            if ($first == true) {
                $retour .= '
							<div class="carousel-item active">
								<img class="d-block w-100 img-slider" src="http://localhost/PPE2-EPSI-2019/image/france/IMAGES/' . $img . '" alt="slide">
  							</div>
  							';
                $first = false;

            } else {
                $retour .= '
							<div class="carousel-item">
								<img class="d-block w-100 img-slider" src="http://localhost/PPE2-EPSI-2019/image/france/IMAGES/' . $img . '" alt="slide">
  							</div>
  							';
            }
        }


        $retour = $retour . '</div>
  <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
';
        return $retour;
    }

    public function retourne_highlight()
    {
        //------------------------- IMAGE ----------------------------------------
        $root = $_SERVER['DOCUMENT_ROOT'];
        $path = "$root/PPE2-EPSI-2019/image/france/IMAGES  RES/";
        $images = array_diff(scandir($path), array('.', '..'));


        //---------------------------------------------------------------------------------
        $retour = '
<div class="container">  <div class="row">';


        $irow = 3;
        foreach ($images as $img) {


            //------------------------- PDO ----------------------------------------

            $result = $this->vpdo->infos_highlight($img);
            $result = $result->fetch(PDO::FETCH_OBJ);

            //------------------------- PDO ----------------------------------------


            if ($irow == 0) {
                $irow = 3;
                $retour .= '</div><div class="row">';
            }

            if ($result) {


                $retour .= '
               <div class="col - sm high">
                    <img class="highlight" src="http://localhost/PPE2-EPSI-2019/image/france/IMAGES  RES/' . $img . '" alt="highlight">
                    <div class="overlay">
                        <div class="text">
                            <h3>  Titre : ' . $result->titre . '</h3>
                            <p>    ' . $result->texte . ' </p>
                            <p> latitude : ' . $result->latitude . '</p>
                            <p> longitude : ' . $result->longitude . '</p>
                        </div>
                    </div>
                </div>';


            } else {


                $retour .= '
               <div class="col - sm high">
                    <img class="highlight" src="http://localhost/PPE2-EPSI-2019/image/france/IMAGES  RES/' . $img . '" alt="highlight">
                    <div class="overlay">
                        <div class="text">
                            <h3>  Titre : inconnue </h3>
                            <p>   pas d\'information  </p>
                            <p> latitude : inconnue</p>
                            <p> longitude : inconnue</p>
                        </div>
                    </div>
                </div>';


            }


        }


        $retour = $retour . '</div></div>';
        //---------------------------------------------------------------------------------
        return $retour;
    }


    public function genererMDP($longueur = 8)
    {
        // initialiser la variable $mdp
        $mdp = "";

        // Définir tout les caractères possibles dans le mot de passe,
        // Il est possible de rajouter des voyelles ou bien des caractères spéciaux
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ&#@$*!";

        // obtenir le nombre de caractères dans la chaîne précédente
        // cette valeur sera utilisé plus tard
        $longueurMax = strlen($possible);

        if ($longueur > $longueurMax) {
            $longueur = $longueurMax;
        }

        // initialiser le compteur
        $i = 0;

        // ajouter un caractère aléatoire à $mdp jusqu'à ce que $longueur soit atteint
        while ($i < $longueur) {
            // prendre un caractère aléatoire
            $caractere = substr($possible, mt_rand(0, $longueurMax - 1), 1);

            // vérifier si le caractère est déjà utilisé dans $mdp
            if (!strstr($mdp, $caractere)) {
                // Si non, ajouter le caractère à $mdp et augmenter le compteur
                $mdp .= $caractere;
                $i++;
            }
        }

        // retourner le résultat final
        return $mdp;
    }

    public function retourne_formulaire_login()
    {
        $retour = '
        
		<div class="modal fade" id="myModal" role="dialog" style="color:#000;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
        				<h4 class="modal-title"><span class="fas fa-lock"></span> Formulaire de connexion</h4>
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="hd();">
          					<span aria-hidden="true">&times;</span>
        				</button>
      				</div>
					<div class="modal-body">
						<form role="form" id="login" method="post">
							<div class="form-group">
								<label for="id"><span class="fas fa-user"></span> Identifiant</label>
								<input type="text" class="form-control" id="id" name="id" placeholder="Identifiant">
							</div>
							<div class="form-group">
								<label for="mp"><span class="fas fa-eye"></span> Mot de passe</label>
								<input type="password" class="form-control" id="mp" name="mp" placeholder="Mot de passe">
							</div>
							<div class="form-group">
								<label class="radio-inline"><input type="radio" name="rblogin" id="rbj" value="rbj">Journaliste</label>
								<label class="radio-inline"><input type="radio" name="rblogin" id="rbr" value="rbr">Rédacteur en chef</label>
								<label class="radio-inline"><input type="radio" name="rblogin" id="rba" value="rba">Administrateur</label>
							</div>
							<button type="submit" class="btn btn-success btn-block" class="submit"><span class="fas fa-power-off"></span> Login</button>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button"  class="btn btn-danger btn-default pull-left" data-dismiss="modal" onclick="hd();"><span class="fas fa-times"></span> Cancel</button>
					</div>
				</div>
			</div>
		</div>';

        return $retour;
    }

    public function retourne_modal_message()
    {
        $retour = '
		<div class="modal fade" id="ModalRetour" role="dialog" style="color:#000;">
			<div class="modal-dialog">
				<div class="modal-content">
				<div class="modal-header">
        				<h4 class="modal-title"><span class="fas fa-info-circle"></span> INFORMATIONS</h4>
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="hd();">
          					<span aria-hidden="true">&times;</span>
        				</button>
      				</div>
		       		<div class="modal-body">
						<div class="alert alert-info">
							<p></p>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" onclick="hdModalRetour();">Close</button>
					</div>
				</div>
			</div>
		</div>
		';

		return $retour;

	}

	public  function  retounr_modal_autres(){
		$retour='
		<div class="table-responsive">
		<table id="xxxx" class="table table-striped table-bordered" cellspacing="0" >
			<thead>
				<tr>
					<th>Code département</th>
					<th>Département</th>
					<th>Région</th>
				</tr>
			</thead>
		<tbody>
		
				';
		return $retour;
	}


}

?>
