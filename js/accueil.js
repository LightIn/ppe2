$(document).ready(function() {

    $(".lire").parents('article').find("p").hide();
    $(".lire").hide();


    $(".lire").click(function () {
        $(this).parents('article').find("p").hide();
        $(".lire").hide();
        $(".cacher").show();
    });
    $(".cacher").click(function () {
        $(this).parents('article').find("p").show();
        $(".lire").show();
        $(".cacher").hide();

    });

});