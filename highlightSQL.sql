-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 17, 2020 at 10:48 PM
-- Server version: 5.6.17-log
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tourisme_france`
--

-- --------------------------------------------------------

--
-- Table structure for table `highlight`
--

DROP TABLE IF EXISTS `highlight`;
CREATE TABLE IF NOT EXISTS `highlight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(200) NOT NULL,
  `titre` varchar(200) NOT NULL,
  `texte` longtext NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `highlight`
--

INSERT INTO `highlight` (`id`, `image`, `titre`, `texte`, `latitude`, `longitude`) VALUES
(1, 'gravures-rupestres-vallee-des-merveilles-region-mont-bego.jpg', 'Gravures rupestres de la vallée des Merveilles et de la région du Mont Bégo à Tende', 'La vallée des Merveilles est une vallée du massif du Mercantour dans les Alpes où ont été découvertes plus de 40 500 gravures rupestres protohistoriques, datant du Chalcolithique et de l’âge du bronze ancien, au milieu d’autres gravures plus récentes\r\nLes gravures rupestres, au nombre d’environ 100 000 dont 37 000 figuratives, concernent 3 700 roches couvrant près de 1 400 ha répartis dans un ensemble plus vaste de 4 000 ha. Elles sont réparties dans sept hautes vallées situées à plus de 2 000 m d’altitude, autour du Mont Bégo (2 872 m) et du Rocher des Merveilles (2 659 m), dont 6 en France : Vallauretta, Valmasque, Col du Sabion, lac Sainte-Marie et surtout Merveilles (972 ha) et Fontanalba (486 ha) remarquables par leur richesse en gravures.\r\n', '44.043', '7.261'),
(2, 'Mont-saint-michel.jpg', 'Mont saint Michel', 'L’architecture du Mont-Saint-Michel et sa baie en font le site touristique le plus fréquenté de Normandie1 et l\'un des dix plus fréquentés en France\r\nEn 1846, Édouard Le Héricher le décrivait ainsi : « Le Mont Saint-Michel apparaît comme une montagne circulaire qui semble s’affaisser sous la pyramide monumentale qui la couronne. On voudrait prolonger sa cime en une flèche aiguë qui monterait vers le ciel (la flèche actuelle ne date que de 1899), dominant son dais de brouillards ou se perdant dans une pure et chaude lumière. De vastes solitudes l’environnent, celle de la grève ou celle de la mer, encadrées dans de lointaines rives verdoyantes ou noires »\r\n', '48.636', '-1.5114'),
(3, 'Bonifacio.jpg', 'Bonifacio', 'La citadelle de Bonifacio est un ouvrage militaire bâti progressivement à partir du xiie siècle pour permettre la protection de Bonifacio qui est une place importante de la République de Gênes pour la sécurité de son commerce entre Gênes, la Ligurie, et la Sardaigne et pour permettre le contrôle des Bouches de Bonifacio.\r\nEnserrée dans ses fortifications, la citadelle médiévale est juchée sur un promontoire de calcaire de plus de 60 m, modelé depuis des millénaires par la mer et le vent.\r\nDans cette citadelle réputée imprenable, l’Histoire se trouve à chaque coin de rue.\r\n', '41.3881', '9.16056'),
(4, 'Gorges-du-Tarn.jpg', 'Gorges-du-Tarn', 'L’un des paysages les plus spectaculaires de France\r\nNé au Mont Lozère, le Tarn descend les Cévennes d’un jet torrentueux qui a creusé un canyon très profond dans la dalle des Grands Causses. Falaises atteignant 500 m de haut, brèches, corniches et rochers … les gorges du Tarn offrent leur plus beau spectacle entre Quézac (Lozère) et Le Rozier (Aveyron), sur environ 50 km.\r\n', '44.3667', '3.4167'),
(5, 'Bassin arcahon.jpg', 'Bassin arcachon', 'La Dune du Pilat\r\nCertainement la dune de sable la plus célèbre au monde, il faut grimper à son sommet pour se rendre compte de sa grandeur ! \r\nLes Cabanes tchanquées de l’île aux Oiseaux\r\nCertains n’ont d’yeux que pour elles, vous comprendrez pourquoi en découvrant les célèbres Cabanes Tchanquées de l’Ile aux Oiseaux.\r\nLe Phare et La Pointe du Cap Ferret\r\nVoici deux lieux mythiques pour mieux comprendre la géographie du Bassin d’Arcachon. Prenez d’abord un peu de hauteur au sommet du Phare, avant d’aller jusqu’à La Pointe et observer la rencontre entre l’eau de l’océan et celle du Bassin d’Arcachon.\r\nLe Banc d’Arguin\r\nEt si vous partiez la journée sur Arguin au milieu du Bassin d’Arcachon… Cap sur la Réserve Naturelle Nationale du Banc d’Arguin, un des joyaux naturels du Bassin d’Arcachon !\r\nLes villages et ports ostréicoles\r\nGujan-Mestras, Andernos, l’Aiguillon, Cap Ferret, l’Herbe, le Canon ou Biganos, certains noms vous disent peut-être quelque chose … Autant de lieux pour flâner, croquer dans quelques huîtres et rencontrer les ostréiculteurs\r\nLa réserve ornithologique du Teich\r\nDes oiseaux sauvages par milliers ! Certaines années, 300 000 oiseaux peuvent transiter par la Réserve ornithologique du Teich, sur le Bassin d’Arcachon\r\n', '44.65', '-1.1667'),
(6, 'chateau-de-chenonceau.jpg', 'chateau-de-chenonceau', 'Le château de Chenonceau est un château de la Loire situé en Touraine, sur la commune de Chenonceaux, dans le département d\'Indre-et-Loire en région Centre-Val de Loire.\r\n\r\nChenonceau avec sa célèbre galerie à deux étages qui domine le Cher est l\'un des fleurons de l\'architecture du Val de Loire. Ses emprunts à l\'Italie et ses caractéristiques françaises sont clairement perceptibles.\r\nPropriété de la Couronne, puis résidence royale, le château de Chenonceau est un site exceptionnel, par sa conception originale, la richesse de ses collections, de son mobilier et de sa décoration, mais aussi par sa destinée, puisqu’il fut aimé, administré et protégé par des femmes, toutes hors du commun et qui, pour la plupart ont marqué l’histoire.\r\n\r\n« Château des Dames » pour l’histoire de France, bâti en 1513 par Katherine Briçonnet, embelli successivement par Diane de Poitiers et Catherine de Médicis, Chenonceau fut sauvé des rigueurs de la Révolution par Madame Dupin. Cette empreinte féminine est partout présente, le préservant des conflits et des guerres pour en faire depuis toujours un lieu de paix.\r\n', '47.3249', '1.07029'),
(7, 'camargue.jpg', 'camargue', 'La Camargue sur une carte forme un triangle coiffé au sommet par Arles et dont la base s’étire le long du littoral entre Le Grau-du-Roi et Salin-de-Giraud, en passant par Les Saintes-Maries-de-la-Mer. Lové dans le delta du Rhône, le parc naturel régional de Camargue offre ses paysages uniques aux amoureux des grands espaces entre marais, étangs, salins, dunes et prairies. \r\n\r\nPARC NATUREL RÉGIONAL DE CAMARGUE : UN MONDE SAUVAGE ET ENVOÛTANT \r\nDans le sud-ouest de la Provence, la Camargue est une région de France hors du commun. Vaste zone humide blottie entre les bras du Rhône, cette terre abrite une faune et une flore exceptionnelles, dont les flamants roses constituent un des symboles forts.\r\nCAMARGUE ET TRADITIONS : LE BERCEAU DES MANADES ET DES GARDIANS\r\nLa Camargue, le cheval et le taureau : une trilogie à découvrir au sein d’une manade. Dans la région, plus d’une centaine d’entre elles font de l’élevage des chevaux Camargue et des taureaux « raço di biou » leur raison d’exister. Spécialement préparés pour les spectacles taurins, ce sont ces redoutables combattants à la robe sombre qu’on retrouve dans les courses camarguaises, les abrivados et les bandidos.\r\n', '43.450001', '4.425'),
(8, 'Orgues_ille_sur_tet.jpg', 'Orgues_ille_sur_tet', 'Les Orgues d\'Ille-sur-Têt sont des cheminées de fée situées sur un site géologique et touristique de la commune d\'Ille-sur-Têt, dans le département français des Pyrénées-Orientales. Elles résultent de l\'érosion de roches sédimentaires vieilles de quatre millions d\'années.\r\ne site des Orgues est un lieu unique en France, un paysage de cheminées de fées à la beauté fragile, éphémère. Il présente aux intempéries des falaises de sables et d’argiles que les pluies ont patiemment ciselées. L’érosion a travaillé comme un artiste, entaillant, incisant, sculptant la matière de balafres ou de stries pour donner à la roche cet aspect écorché, presque lunaire.\r\n', '42.67772091', '2.612058944'),
(9, 'Cirque-de-gavarnie.jpg', 'Cirque-de-gavarnie', 'CIRQUE DE GAVARNIE, LE COLOSSE DES PYRÉNÉES\r\nGrandiose ! Comment qualifier autrement le cirque de Gavarnie ? Les grands cirques calcaires pyrénéens doivent leur existence au travail d\'immenses glaciers aujourd\'hui disparus. Gavarnie, dans les Hautes-Pyrénées, est le plus célèbre d\'entre eux.\r\nIl y a cinquante millions d’années, érosions fluviales et glaciaires ont creusé dans les Pyrénées un ensemble de cirques naturels d’une rare perfection. Gavarnie est le plus célèbre, le plus spectaculaire aussi. Ce colosse de la nature entouré de seize sommets de plus de 3 000 mètres et abritant la plus haute cascade d’Europe est d’une beauté étourdissante. Le spectacle est saisissant, l’émotion garantie.  Adossés aux grands canyons du Haut-Aragon espagnol, ils forment ensemble le site de « Gavarnie-Mont Perdu », une montagne magique inscrite au Patrimoine mondial de l’humanité par l’UNESCO.\r\n', '42.73333', '-0.00833'),
(10, 'carcassonne.jpg', 'Château comtal de la cité de Carcassonne', 'La cité médiévale de Carcassonne est l’une des plus belles cités médiévales du monde, classée aux Monuments Historiques et inscrite au Patrimoine mondial de l’Unesco.\r\nUn monument grandiose !\r\n\r\nLa Cité est un ensemble fortifié unique en Europe et très complet : 3 km de remparts, 52 tours, un château, véritable forteresse dans la forteresse, une basilique, et un village toujours habité.\r\nCarcassonne existe depuis près de 2500 ans, successivement villa romaine, vicomté médiévale sous la dynastie des Trencavels, victime d’une terrible croisade, détruite, reconstruite, agrandie et renforcée, abandonnée au cours des siècles, puis sauvée de la destruction et enfin restaurée par l’œuvre majeure de Viollet-le-Duc au 19e siècle\r\n\r\n', '43.2073', '2.3633');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
